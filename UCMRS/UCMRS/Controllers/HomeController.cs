﻿using System;
using System.Linq;
using System.Web.Mvc;
using UCMRS.Models.Entity;
using UCMRS.Models.UnivContext;
using UCMRS.Models.View;

namespace UCMRS.Controllers
{
    public class HomeController : Controller
    {
        UniContext db = new UniContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetMenuList()
        {
            try
            {
                ViewBag.MainMenu = db.Menus.ToList();
                ViewBag.SubMenu = db.SubMenus.ToList();

                return PartialView("Menu");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }  

        public ActionResult IndexMainMenu()
        {
            try
            {
                ViewBag.MainMenu = db.Menus.ToList();
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMainMenu(MainMenu objMainMenu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Menus.Add(objMainMenu);
                    db.SaveChanges();
                }
                return RedirectToAction("IndexMainMenu", objMainMenu);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet]
        public ActionResult DeleteMainMenu(int? id)
        {
            var mm = db.Menus.Find(id);
            if (mm != null)
            {
                 db.Menus.Remove(mm);
                var g = db.SaveChanges();
            }
            return RedirectToAction("IndexMainMenu");
        }

        public ActionResult SubMenuIndex()
        {
            try
            {
                Vm_SubMenu vSub = new Vm_SubMenu();
                ViewBag.SubMenu = vSub.ListVmSubMenu();
                ViewBag.MainMenu = new SelectList(db.Menus.ToList().OrderBy(x => x.MenuId), "MenuId", "MenuName");
                return View();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public ActionResult CreateSubMenu(SubMenu objSubMenu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.SubMenus.Add(objSubMenu);
                    db.SaveChanges();
                }
                return RedirectToAction("SubMenuIndex", objSubMenu);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}